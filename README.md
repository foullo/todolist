# todolist

#html part
<input id="input" placeholder="TodoList">
    <button id="btn_add" value="Ajouter"type="button">Ajouter</button>
    <button id="btn_sup" value="supp" type="button">Supprimer</button>
      <ul id="list"></ul>

      
#js part
var button = document.getElementById("btn_add");
    button.addEventListener('click', newItem);
function newItem() {
  var item = document.getElementById("input").value;
  var ul = document.getElementById("list");
  var li = document.createElement("li");
  li.appendChild(document.createTextNode(" - " + item));
  ul.appendChild(li);
  document.getElementById("input").value = "";
  li.onclick = removeItem;
    
}


document.body.onkeyup = function(e) {
  if (e.keyCode == 13) {
    newItem();
  }
};


function removeItem(e) {
  e.target.parentElement.removeChild(e.target);

}
